extends Node

class_name Audio

const BUFFER_SIZE = 0.05

static func set_constants(node: Node):
	# we set the buffer size of all audio stream players
	for x in node.get_tree().get_nodes_in_group("ASP"):
		var asp : AudioStreamPlayer3D=x as AudioStreamPlayer3D 
		var s : AudioStreamGenerator = asp.stream 
		s.buffer_length = BUFFER_SIZE
