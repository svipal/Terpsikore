extends Node

class_name RMEntry


enum IconMode {
	Never,
	Highlighted,
}

@export var color = Color.WHITE
@export var iconMode :int = 0
@export var icon : Texture2D  = Texture2D.new()
@export var weight : int = 1
@export var action: Callable = func(): return null

func setWeight(w : int):
	weight = w

func setColor(c):
	color = c

func setAction(a: Callable):
	action = a

func setIcon(ic : Texture2D):
	icon = ic
