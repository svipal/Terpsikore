extends Node3D

class_name Character

var coords = Vector2()

const highlight_material = preload("res://Graphics/materials/highlight.material")

signal char_clicked(char)
signal char_hovered(char,hovered)

var buffer = PackedVector2Array([])


enum CState {
	Idle,
	Attacking,
	Reeling,
	Defending,
	Running
}

@export var state : CState = CState.Idle

@export var cclass : Class = Classes.default

@export var hovered = false

@export var selected = true

func defineClass(cname):
	cclass = Classes.getClass(cname)
	var model  : CharModel = LoadChar.models[cname].instantiate()
	model.name = "model"
	add_child(model)
	lateInit()
	
	
func noteOn():
	var beat = create_tween()
	beat.set_process_mode(Tween.TWEEN_PROCESS_PHYSICS)
	beat.tween_interval(Audio.BUFFER_SIZE)
	beat.tween_property(self,"rotation:x",0.12,0.06)
	beat.parallel().tween_property(self,"rotation:z",randf()*0.4-0.2,0.06)
	beat.play()

	
func noteOff():
	var beat = create_tween()
	beat.set_process_mode(Tween.TWEEN_PROCESS_PHYSICS)
	beat.tween_interval(Audio.BUFFER_SIZE)
	beat.tween_property(self,"rotation:x",0,0.06)
	beat.parallel().tween_property(self,"rotation:z",0,0.06)
	beat.play()
	
func state_anim():
	match state:
		CState.Idle:
			$model/AnimationPlayer.play("Idle")

func place(cell):
	var pixel  = Hex.hexToPixel(cell)
	var origin : Vector3 = Vector3(pixel.x, 0, pixel.y)
	transform.origin = origin
	coords = cell

func lateInit():
	$model.scale_object_local(Vector3(0.25,0.25,0.25))
	$model.transform.origin+=Vector3(0,0.05,0)
	state_anim()
#	$player.bus=
	
var postInitialized = false

func postInit():

	postInitialized = true

func get_playback():
	if !$player.playing:
		$player.play()
	return $player.get_stream_playback()

func get_cname():
	return cclass.name

func overlayHighlights(on : bool):
	var highlightOverlay =  highlight_material if on else null
	H.recurApplyTo($model, func(node): if node is MeshInstance3D: node.material_overlay = highlightOverlay, true)
var curkeys  =[]
func _input(event):
	if event is InputEventMouseButton and event.pressed and hovered:
		char_clicked.emit(self)


func _on_pick_area_mouse_entered():
	hovered=true
	overlayHighlights(true)
	print(self)
	char_hovered.emit(self,true)

func _on_pick_area_mouse_exited():
	hovered=false
	overlayHighlights(false)
	char_hovered.emit(self,false)
