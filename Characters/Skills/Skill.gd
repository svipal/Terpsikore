extends Node

class_name Skill
 
## how many ticks until this can be used again
@export var tick_cooldown : int = 0
## how many turns until this can be used again
@export var turn_cooldown : int = 0
## how many ticks until this move goes off
@export var tick_casting  : int = 0
## how many ticks until you go back to waiting position
@export var tick_recovery : int = 0

var resource_costs : Dictionary = {}
var special_tags : Dictionary = {}


#the return array is an Array of Array<Actor>. each representing a potential choice for the targetting system
func target_system(s: Scene, user: Actor) -> Array:
	return []
	
# the finalized array is an Array<Actor>, representing the final choice
# the return array is an Array[Actor,Effect]
# this function finalizes the effects and returns them to be put into the effects queue.
func bake_effects( s: Scene, user : Actor, finalized_targets:Array) -> Array:
	return []

func skill_animation(s: Scene, user: Actor) -> void:
	pass


func use(s: Scene, user: Actor,targets):
	skill_animation(s,user)
	return bake_effects(s,user,targets)
