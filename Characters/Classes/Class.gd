extends Node

class_name Class

@export var start : int = 0
@export var end : int = 15
@export var sf : String = ""

@export var instrument : int = 40

@export var instruments : Dictionary = {}

@export var base_stats : Dictionary = {
}

@export var growth: Dictionary = {
}

@export var skills : Dictionary = {
}

func _init(n:String,s,e,sfnt):
	name=n
	start=s
	end=e
	sf=sfnt
	
func fromClass(mod:Callable):
	return mod.call(self.duplicate())

func withMods(baseMods, grwMods):
	return fromClass(func(cl):
			for key in baseMods.getKeys():
				var stat = baseMods[key]
				cl.baseStats[key]+=stat
			for key in grwMods.getKeys():
				var stat = baseMods[key]
				cl.growStats[key]+=stat)

func withInstr(new_instr: Dictionary):
	var oc : Class = self.duplicate()
	oc.instruments = new_instr
	return oc 	
