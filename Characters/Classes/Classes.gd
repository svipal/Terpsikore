extends Object

class_name Classes

func test():
	print(self.get_property_list())

static func getClass(cname):
	match cname:
		"rogue":
			return rogue
		"knight":
			return knight
		"mage":
			return mage
		"blade":
			return sorc

static var default = rogue
static var sorc = 	Class.new("blade",0,5,"2.sf2")
static var rogue = 	Class.new("rogue",5,6,"2.sf2")
static var mage = 	Class.new("mage",6,9,"3.sf2")
static var knight = Class.new("knight",10,15,"1.sf2")

