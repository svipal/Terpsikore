extends Sprite2D


var coords = Vector2i(0,0)

func place():
	position = coords * 16 

func _input(event):
	# we first declare the target variable
	var target = coords
	# we change the value of the target depending on the direction we're going in 
	if event.is_action_pressed("ui_down"):
		target = coords + Vector2i(0,1)
	if event.is_action_pressed("ui_left"):
		target = coords + Vector2i(-1,0)
	if event.is_action_pressed("ui_right"):
		target = coords + Vector2i(1,0)
	if event.is_action_pressed("ui_up"): 
		target = coords + Vector2i(0,-1)

	#we check if the targeted cell is walkable.
	var cell_data : TileData = $"../TileMap".get_cell_tile_data(0,target)
	# if the cell exists (we are not trying to walk into the void)
	# and if the  cell is walkable (as defined by our tileset painting of the walkable custom data earlier)
	if cell_data != null and cell_data.get_custom_data("Walkable") == 1:
		# then we snap the player to the targetted coordinates
		coords = target
		place()
