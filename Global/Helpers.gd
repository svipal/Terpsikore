extends Object

class_name H

static func recurApplyTo(node : Node ,function : Callable, childrenFirst = true):
	if not childrenFirst:
		function.call(node)
	for childNode in node.get_children():
		recurApplyTo(childNode, function, childrenFirst)
	if childrenFirst:
		function.call(node)
