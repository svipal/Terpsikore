extends Node

class_name Hex

const hex_size = 0.50

static func axial_round(frac) -> Vector2i :
	var q = round(frac.x)
	var r = round(frac.y)
	var s = round(frac.z)

	var q_diff = abs(q - frac.x)
	var r_diff = abs(r - frac.y)
	var s_diff = abs(s - frac.z)

	if q_diff > r_diff and q_diff > s_diff:
		q = -r-s
	elif r_diff > s_diff:
		r = -q-s
	else:
		s = -q-r

	return Vector2(q, r)


static func pixToHex(point):
	var q = (sqrt(3)/3 * point.x  -  1./3 * point.y) / hex_size
	var r = (                        2./3 * point.y) / hex_size
	return axial_round(Vector3(q, r, -q-r))
	
static func hexToPixel(hex):
	var x = hex_size * (sqrt(3) * hex.x  +  sqrt(3)/2 * hex.y)
	var y = hex_size * (                         3./2 * hex.y)
	return Vector2(x, y)
