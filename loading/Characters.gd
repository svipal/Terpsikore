extends Node

class_name LoadChar



const models = {
	"knight":preload("res://Characters/modelscenes/knight.tscn"),
	"rogue":preload("res://Characters/modelscenes/rogue.tscn"),
	"fox":preload("res://Characters/modelscenes/fox.tscn"),
	"mage":preload("res://Characters/modelscenes/mage.tscn"),
	"blade":preload("res://Characters/modelscenes/sorc.tscn"),
}
