extends Node2D

#the purpose of the position is to allow multiple radial menus to chain together with the same center
signal selected_item(index,entry,position)
signal highlighted_item(index,entry,position)


@export var index : int = -1

# ray of the radial menu in pixels
@export var size= 150

@export var icon_centered = false

#how wide the circle is
@export var width = 25

# how close to a circle the radialmenu is
@export var grain : int = 5 

@onready var entries :Array[RMEntry] = buildEntries() 

func buildEntries() -> Array[RMEntry]:
	var allEntries: Array[RMEntry] = []
	for entry in $Entries.get_children():
		allEntries.append(entry as RMEntry)
	return allEntries
	
var base_mouse_position = Vector2()

var tiers = []

func _ready():
	$TierProto.width = width
	
func buildMenu():
	var start = 0
	var end = 0
	var total = 0
	for entry in entries:
		total += entry.weight*grain
	for entry in entries:
		start = end
		end = start + entry.weight*grain
		tiers.append([float(start)*PI*2/float(total),float(end)*PI*2/float(total)])
		var line : Line2D = $TierProto.duplicate()
		line.show()
		#yeah
		if entry.color and false:
			line.default_color = entry.color
		else:
			var rng = RandomNumberGenerator.new()
			rng.seed = hash(entry.name)
			line.default_color=Color(rng.randf(),rng.randf(),rng.randf())
		for i in range(start,end):
			line.add_point(size*Vector2(cos(float(i)/float(total)*PI*2), sin(float(i)/float(total)*PI*2)))
		line.add_point(size*Vector2(cos(float(end)/float(total)*PI*2), sin(float(end)/float(total)*PI*2)))
		$Tiers.add_child(line)

var base_mouse = Vector2()
var final_mouse = Vector2()

func appear(pos):
	position = pos
	show()
	base_mouse = get_viewport().get_mouse_position()

func fade():
	hide()

func trigger():
	final_mouse = get_viewport().get_mouse_position()
	var delta_mouse = final_mouse-base_mouse
	var angle = fmod(delta_mouse.angle() + PI*2,PI*2)
	if delta_mouse.length() >= size:
		var tier = identifyTierIndex(angle)
		print(entries[tier].name, "lets go")
		selected_item.emit(tier,entries[tier])
	hide()

func identifyTierIndex(angle):
	var i = 0
	for tier in tiers:
		var start = tier[0]
		var end = tier[1]
		if angle >= start and angle <= end:
			return(i)
		i += 1


@export var angle = 0
@export var delta_mouse = Vector2()
func _input(event):
	if visible and event is InputEventMouseMotion:
		refresh()
		
		
var frameMod = 0
		
		
func _process(_delta):
	frameMod = frameMod + 1 % 3
	if frameMod  ==0:
		refresh()
		
func refresh():
	final_mouse = get_viewport().get_mouse_position()
	delta_mouse = final_mouse-base_mouse
	$Trait.points[1] = delta_mouse/2
	if delta_mouse.length() >= size:
		angle = fmod(delta_mouse.angle() + PI*2,PI*2)
		var tier = identifyTierIndex(angle)
		#if we changed tiers
		if tier != index:
			highlighted_item.emit(tier,entries[tier],position)
			if entries[tier].iconMode == RMEntry.IconMode.Highlighted:
				$CurrentSprite.texture = entries[tier].icon
				# if we didn't select anything before 
				if index ==-1: U2D.slowShow($CurrentSprite)
				var midAngle = (tiers[tier][0] + tiers[tier][1]) / 2.0
				if !icon_centered: $CurrentSprite.position = Vector2(cos(midAngle),sin(midAngle))*size
			index = tier
	else :
		index = -1
		U2D.slowHide($CurrentSprite)

var octave=5

func _on_d_pressed():
	octave -=1
	refresh()

func _on_w_pressed():
	octave +=1
	refresh()
