extends Ensemble

@onready var mp: MidiPlayer  = MidiPlayer.new()
var pauser
var ender

var characters : Dictionary = {}

func setPauserAndEnder():
	print("we are here")
	pauser = mp.getPauser()
	ender = mp.getEnder()

# Called when the node enters the scene tree for the first time.
func loadMusicians(musicians):
	for m in musicians :
		print(m.cclass.name)
		var name = m.cclass.name
		var start = m.cclass.start
		var end = m.cclass.end
		var sf = m.cclass.sf
		characters[name] = m
		loadSoundfont(str(name),start,end,sf)

func _ready():
	mp.playbackEnded.connect(end)
	mp.setCallbacks(on_note_on,on_note_off,on_pitch_bend,on_cc,on_pc_change)
	loadSoundfont("drums",9,10,"4.sf2")
##	loadSoundfont("knight2",9,12,"ZPH.sf2")
#	loadSoundfont("rogue",5,6,".sf2")
#	loadSoundfont("mage",6,7,"ZPH.sf2")

func on_cc(channel,controller,value):
	call_thread_safe("sendCC",channel,int(controller),int(value))

func on_pitch_bend(channel,msb,lsb):
#	print("pitch bend : ",channel," ", msb, " ",lsb)
	call_thread_safe("pitchBend",channel,int(msb),int(lsb))

func on_note_on(channel,key,velocity):
#	print("note on : ",channel," ", key, " ",velocity)
	#channel 10 is always the beat channel
	call_thread_safe("noteOn",channel,key,velocity)
	if channel == 9:
		get_parent().call_deferred("handleBeat",key, velocity)
	else:
		for ch in characters.values():
			if ch.cclass.start <= channel and ch.cclass.end > channel:
				ch.call_deferred("noteOn")

func on_note_off(channel,key,velocity):
	call_thread_safe("noteOff",channel,key,velocity)
	if channel != 9:
		for ch in characters.values():
			if ch.cclass.start <= channel and ch.cclass.end > channel:
				ch.call_deferred("noteOff")

func on_pc_change(channel,pc):
#	print("pc change",channel," ",pc)
	call_thread_safe("setInstrument",channel,pc)

func pause(paused):
	if pauser!=null:
		pauser.send()
		return true
	else:
		return false
	
func end ():
	if ender:
		ender.send()
#	th.wait_to_finish()
	th = Thread.new()
	
	
var th = Thread.new()

func start(midifilename):
	mp.prepareFilePlayback(midifilename)
	setPauserAndEnder()
	th.start(mp.startPlayback)
