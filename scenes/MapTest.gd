extends Node3D

const DIR_LAYER = 3
const UNIT_LAYER = 2
const FACTION_LAYER = 1
const BIOME_LAYER = 0

var baseCharacter : PackedScene= preload("res://Characters/Character.tscn")
var mp


func _ready():
	getMidis()
	$MidiList.select(0)
	$MidiList.item_selected.emit(0)
#	Audio.set_constants(self)
	$%PreMap.hide()
	$%Radial.buildMenu()
	buildMap()
	var units = placeUnits()
	var grid_mat : ShaderMaterial= $MMProto.material_overlay
	var ms = get_tree().get_nodes_in_group("characters")
	$Ensemble.loadMusicians(ms as Array[Character])
func buildMap():
	for cell in $PreMap.get_used_cells(BIOME_LAYER):
		var data : TileData = $PreMap.get_cell_tile_data(BIOME_LAYER,cell)
		var biomeName =  data.get_custom_data("biome")
		# we check if there is already a container for this biome
		var biomeContainer : MultiMeshInstance3D = $Biomes.get_node(biomeName)
		if biomeContainer == null:
			print("current biome (",biomeName,") not found in available containers , preparing a new one...")
			# we use our prototype as a base
			biomeContainer = $MMProto.duplicate(true)
			biomeContainer.show()
			biomeContainer.multimesh = $MMProto.multimesh.duplicate(true)
			biomeContainer.name = biomeName
			$Biomes.add_child(biomeContainer)
			var tile = $TileLib.get_node(biomeName)
			if tile == null:
				print("build catastrophe : couldn't find ",biomeName, " in available biomes")
				return
			biomeContainer.multimesh.mesh = tile.mesh
			print("tile container made:", biomeContainer)
			# we use custom properties so each container keeps tracks of its index.
			biomeContainer.set_meta("index",0)
			#if we don't do this following line the buffer is filled with random crap wtf
			biomeContainer.multimesh.set_instance_count(200)
		
		
		#2D TileMap to 3D MMesh instance transform
		var pixel  = Hex.hexToPixel(cell)
#		print(pixel)
		var origin : Vector3 = Vector3(pixel.x, 0 , pixel.y)
		if biomeName in $TileLib.attached_particles:
			var pgen = $TileLib.attached_particles[biomeName].duplicate()
			pgen.show()
			pgen.transform.origin += origin
			$ParticleGenerators.add_child(pgen)
			
		var iindex = biomeContainer.get_meta("index")
		biomeContainer.set_meta("index",iindex+1)
		
		biomeContainer.multimesh.set_instance_transform(iindex,Transform3D(Basis(),origin))
#		print(biomeContainer,biomeContainer.multimesh.instance_count)

func placeUnits():
	var units = []
	for cell in $PreMap.get_used_cells(UNIT_LAYER):
		var faction_data : TileData = $PreMap.get_cell_tile_data(FACTION_LAYER,cell)
		var unit_data: TileData = $PreMap.get_cell_tile_data(UNIT_LAYER,cell)
		var factionName =  faction_data.get_custom_data("faction")
		var unitClass =  unit_data.get_custom_data("unit")
		var unit : Character = baseCharacter.instantiate()
		unit.defineClass(unitClass)
		unit.connect("char_clicked",select)
		if unit == null:
			print("failed to load unit...")
			break
		#we place the unit
		
		unit.place(cell)
		units.append(unit)
		unit.name = unitClass
		var factionContainer = $Factions.get_node(factionName)
		if factionContainer:
			factionContainer.add_child(unit)
		else:
			factionContainer = Node3D.new()
			factionContainer.name = factionName
			$Factions.add_child(factionContainer)
			factionContainer.add_child(unit)
		unit.postInit()
		#DIRTY: hack to get all instruments
		if factionName=="browsers":
			print("browsers detected")
			unit.cclass.name="knight2"
			unit.cclass.start=6
			unit.cclass.end=7
		var dir_data: TileData = $PreMap.get_cell_tile_data(DIR_LAYER,cell)
		if dir_data:
			var dir :int =  dir_data.get_custom_data("direction")
			unit.rotate_y(dir*PI/3)
	return units
var selected = null

func select(unit : Character):
	print("selected : ",unit.cclass.name, "@", unit.coords)
	selected = unit
	$select.transform.origin = unit.transform.origin+Vector3(0,0.1,0)
	$select.show()
	channel = unit.cclass.start

var menu : bool = true
var follow: bool = false
var rot: bool = false
var singing: bool = false
var old_angle = 0
var channel = 0
var note = 0
var old_note = 0
var old_delta = 0

func _input(event):
	if event is InputEventKey:
		if event.keycode == KEY_SHIFT:
			if event.pressed and not $%Radial.visible and not event.echo:
				menu=true
				$%Radial.appear(get_viewport().get_mouse_position())
			elif not event.pressed and  not event.echo:
				menu=false
#				$Ensemble.all_notes_off()
#				reset pitch bend
				for i in 16:
					$Ensemble.on_pitch_bend(i,64,64)
				$Ensemble.on_note_off(channel,old_note,min(126,int($%Radial.delta_mouse.length()/2)))
				$Ensemble.on_note_off(channel,note,min(126,int($%Radial.delta_mouse.length()/2)))
				$%Radial.fade()
		elif menu and event.keycode==KEY_S or event.keycode == KEY_SPACE:
			if event.pressed and not singing and not event.echo:
				$%SlideMenu.show()
				singing = true
				old_angle = $%Radial.angle
				old_delta=$%Radial.delta_mouse.length()
				old_note=note
				#reset volume control and pitch bend on new inputted note
				$Ensemble.on_cc(channel,7,96)
				$Ensemble.on_pitch_bend(channel,63,63)
				#note starts
				$Ensemble.on_note_on(channel,note,min(126,int($%Radial.delta_mouse.length()/2)))
			elif not event.pressed and not event.echo:
				singing = false
				$%SlideMenu.hide()
				$Ensemble.on_note_off(channel,old_note,min(126,int($%Radial.delta_mouse.length()/2)))
	else:
		if event is InputEventMouseButton:
			if event.button_index==MOUSE_BUTTON_LEFT:
				if event.pressed: 
					$Ensemble.on_note_on(9,35,100)
				else:
					$Ensemble.on_note_off(9,35,100)
			if event.button_index==MOUSE_BUTTON_RIGHT:
				follow = event.pressed
			if event.button_index==MOUSE_BUTTON_MIDDLE:
				rot = event.pressed
			if event.button_index==MOUSE_BUTTON_WHEEL_UP:
				$CH/Camera3D.position.z -= 0.1
			if event.button_index==MOUSE_BUTTON_WHEEL_DOWN:
				$CH/Camera3D.position.z += 0.1
		if event is InputEventMouseMotion:
			if follow:
				var tf = transform.rotated(Vector3.UP,$CH.rotation.y )
				$CH.global_translate(-Vector3(
					tf.basis.x*event.relative.x * 0.05))
				$CH.global_translate(-Vector3(
					tf.basis.z*event.relative.y * 0.05))
			if rot:
				$CH.rotation.y -= event.relative.x * 0.07
				$CH.rotation.x -= event.relative.y * 0.07
#			if singing:
#				#old midi pitchbend calculations
#				var angle_diff = fposmod(old_angle- $%Radial.angle   + 3*PI,2*PI)
#				var delta_diff=  $%Radial.delta_mouse.length() - old_delta
#				var midi_pb = (int((angle_diff*128 / (2*PI)) ))


func _on_radial_selected_item(_index : int, entry : RMEntry, _position : Vector2):
	entry.action.call()

var pause = false
func _process(_delta):
	if not $Drums.playing:
		$Drums.play()
	var pbs = [[StringName("drums"),$Drums.get_stream_playback()]]
	for c in get_tree().get_nodes_in_group("characters"):
		var n = c.get_cname()
		var pb = c.get_playback()
		pbs.push_front([n,pb])
	
	if not pause :
		$Ensemble.render_separate(pbs)
	
func handleBeat(key, velocity):
	var beat = create_tween()
#	beat.set_process_mode(Tween.TWEEN_PROCESS_PHYSICS)
	beat.tween_interval(Audio.BUFFER_SIZE)
	beat.tween_property($Lighting.get_child(key%2),"light_energy",(key %10)*0.1,0.06)
#	beat.parallel().tween_property($WorldEnvironment,"environment:tonemap_exposure",0.752,0.06)
	beat.parallel().tween_property($Biomes.get_child(key%$Biomes.get_child_count()),"position:y",0.05,0.06)
	beat.tween_property($Lighting/Backlight,"light_energy",2.5,0.16)
#	beat.parallel().tween_property($WorldEnvironment,"environment:tonemap_exposure",0.63,0.06)
	beat.parallel().tween_property($Biomes.get_child(key%$Biomes.get_child_count()),"position:y",0,0.260)
	beat.play()
	
func _on_radial_highlighted_item(index, entry, position):
	note = posmod(-index,12) + $%Radial.octave*12
#	print(note)
	pass # Replace with function body.

func _on_pause_toggled(button_pressed):
	pause = button_pressed
	var succ = $Ensemble.pause(button_pressed)
	if not succ:
		pause = not button_pressed
		$PAUSE.set_pressed_no_signal(not button_pressed)
func _on_end_pressed():
	$Ensemble.allNotesOff()
	$Ensemble.end()
	$PAUSE.set_pressed_no_signal(false)
	pause = false

func _on_start_pressed():
	$Ensemble.start(selectedMidi)

var selectedMidi = ""

func getMidis():
#	var midis = []
	var dir = DirAccess.open(".")
	if dir:
		dir.list_dir_begin()
		var file_name = dir.get_next()
		while file_name != "":
			if dir.current_is_dir():
				print("Found directory: " + file_name)
			else:
				print("Found file: " + file_name)
				if file_name.ends_with(".mid"):
#					midis.push_back(file_name)
					$MidiList.add_item(file_name)
			file_name = dir.get_next()
	


func _on_midi_list_item_selected(index):
	selectedMidi = $MidiList.get_item_text(index)



func _on_slide_menu_send_vector(v):
		$Ensemble.on_pitch_bend(channel,63,int(-v.y/1.5+63))
		$Ensemble.on_cc(channel,7,min(126,int(v.x/1.5+63)))

