extends Node2D

signal sendVector(v:Vector2)

var center_mouse = Vector2()

func appear():
	center_mouse = get_viewport().get_mouse_position()
	global_position = center_mouse
		
var frameMod = 0
		
		
func _process(_delta):
	frameMod = frameMod + 1 % 3
	if frameMod  ==0 and visible:
		refresh()

var old_mouse = Vector2()
func refresh():
	var current_mouse =  get_viewport().get_mouse_position()
	if old_mouse != current_mouse:
		var delta_mouse = current_mouse- center_mouse
		sendVector.emit(delta_mouse)

func _on_visibility_changed():
	if visible:
		appear()
