extends Node

class_name Util2D

func slowShow(node: Node2D, time=0.25):
	var tween: Tween = create_tween()
	node.self_modulate.a = 0
	node.show()
	tween.tween_property(node,"self_modulate:a",1.0,time)
	

func slowHide(node: Node2D, time=0.25):
	var tween: Tween = create_tween()
	node.self_modulate.a = 1
	tween.tween_property(node,"self_modulate:a",0.0,time)
	
	
